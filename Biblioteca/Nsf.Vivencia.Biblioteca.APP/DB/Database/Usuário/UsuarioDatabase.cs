﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Database.Usuário
{
    public class UsuarioDatabase
    {
        public int Save (tb_usuario dto)
        {
            BibliotecaDB contex = new BibliotecaDB();
            contex.tb_usuario.Add(dto);
            return contex.SaveChanges();
        }
        public void Remove (int id)
        {
            BibliotecaDB context = new BibliotecaDB();
            tb_usuario user = context.tb_usuario.First(x => x.id_usuario == id);
            context.tb_usuario.Remove(user);
            context.SaveChanges();
        }
        public void Update (tb_usuario id)
        {
            BibliotecaDB context = new BibliotecaDB();
            tb_usuario user = context.tb_usuario.First(x => x.id_usuario == x.id_usuario);
            context.SaveChanges();
        }
        public List<tb_usuario> List ()
        {
            BibliotecaDB context = new BibliotecaDB();
            return context.tb_usuario.ToList();
        }
    }
}
