﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Novas_Telas.Consultar;

namespace WindowsFormsApplication1.Novas_Telas.Alterar
{
    public partial class AlteraçãoDevolucao : UserControl
    {
        public AlteraçãoDevolucao()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultaDevolucao frm = new ConsultaDevolucao();
            frm.Show();
            Hide();
        }
    }
}
