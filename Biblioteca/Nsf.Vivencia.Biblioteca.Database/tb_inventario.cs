//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nsf.Vivencia.Biblioteca.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_inventario
    {
        public int id_inventario { get; set; }
        public Nullable<int> ds_mes { get; set; }
        public Nullable<int> ds_perda { get; set; }
        public Nullable<int> ds_entrega { get; set; }
        public Nullable<int> ds_reposicoes { get; set; }
        public Nullable<int> ds_emprestimos { get; set; }
        public Nullable<int> ds_doacoes { get; set; }
        public Nullable<int> ds_pesquisa { get; set; }
    }
}
