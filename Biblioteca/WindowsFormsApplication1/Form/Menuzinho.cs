﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.NovasTelas;
using WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar;
using WindowsFormsApplication1.User_Controls.Emprestimo;

namespace WindowsFormsApplication1.Telas.NewFolder1
{
    public partial class MenuSecundário : Form
    {
        public static MenuSecundário Atual;

        public MenuSecundário()
        {
            InitializeComponent();
        }

        #region Open Screen
        public void OpenScreen(UserControl control)
        {
            // Se a quantidades de CONTROLES for igual a 1, Ou seja, se já tiver uma tela dentro da GROUP BOX.
            if (PanelPrincipal.Controls.Count == 1)
                PanelPrincipal.Controls.RemoveAt(0);
            PanelPrincipal.Controls.Add(control);
            //Eu irei REMOVER o primeiro ÍNDICE do CONTROLE
            //Ou seja, sempre quando eu for abrir uma tela nova, ele vai remover a tela ATUAL e ADICIONA a NOVA.
        }

        #endregion

        #region BotõesTela
           
        

       

        #endregion
        
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void MenuSecundário_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnexit_Click(object sender, EventArgs e)
        {
            DialogResult re = MessageBox.Show("Biblioteca",
                                                "Deseja realmente fechar o programa?",
                                                MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question);
            if (re == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btn__Click(object sender, EventArgs e)
        {
           
        }

        private void btnLivro_Click_1(object sender, EventArgs e)
        {
            frmLivro tela = new frmLivro();
            OpenScreen(tela);
            //SidePanel.Height = btnLivro.Height;
            //SidePanel.Top = btnLivro.Top;
        }

        private void btnAcervo_Click(object sender, EventArgs e)
        {
           //consulta

            //SidePanel.Height = btnAcervo.Height;
            //SidePanel.Top = btnAcervo.Top;

        }

        private void btnUsuario_Click(object sender, EventArgs e)
        {
            frmUsuario tela = new frmUsuario();
            OpenScreen(tela);
            //SidePanel.Height = btnUsuario.Height;
            //SidePanel.Top = btnUsuario.Top;

        }

        private void btnInventario_Click(object sender, EventArgs e)
        {
            frmInventario tela = new frmInventario();
            OpenScreen(tela);

            //SidePanel.Height = btnAcervo.Height;
            //SidePanel.Top = btnAcervo.Top;

        }

        private void btnDevolucao_Click(object sender, EventArgs e)
        {
            //???

            //SidePanel.Height = btnDevolucao.Height;
            //SidePanel.Top = btnDevolucao.Top;
        }

        private void btnEmprestimo_Click(object sender, EventArgs e)
        {
            frmEmprestimo tela = new frmEmprestimo();
            OpenScreen(tela);
            //SidePanel.Height = btnEmprestimo.Height;
            //SidePanel.Top = btnEmprestimo.Top;

        }

        private void button8_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Peça auxilio ao Administrador do Sistema, /n " +
                "no momento estamos sem opções de ajuda cadastradas nessa versão.",
                "Biblioteca",MessageBoxButtons.OK, MessageBoxIcon.Information
                );
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
