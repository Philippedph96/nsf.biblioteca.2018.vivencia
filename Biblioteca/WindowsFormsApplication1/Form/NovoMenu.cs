﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Novas_Telas.Alterar;

namespace WindowsFormsApplication1.Novas_Telas.Inícios
{
    public partial class NovoMenu : Form
    {
        public static NovoMenu Atual;
        public NovoMenu()
        {
            InitializeComponent();
        }

        public void OpenScreen (UserControl control)
        {
            if (pnlConteudo.Controls.Count == 1)
                pnlConteudo.Controls.RemoveAt(0);
            pnlConteudo.Controls.Add(control);
            
        }

        private void NovoMenu_Load(object sender, EventArgs e)
        {
            AlteraçãoEmprestimo tela = new AlteraçãoEmprestimo();
            OpenScreen(tela);
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }
    }
}
