﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Business;

namespace WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar
{
    public partial class frmUsuario : UserControl
    {
        public frmUsuario()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            tb_usuario dto = new tb_usuario();
            dto.nm_nome = textBox1.Text;
            dto.ds_ano = Convert.ToInt32(txtAno.Text);
            dto.ds_telefone = txtTelefone.Text;
            dto.ds_celular = txtCelular.Text;
            dto.ds_numero = txtNumero.Text;
            dto.dt_nascimento = Convert.ToDateTime(txtNascimento.Text);
            dto.ds_ra = txtRA.Text;
            dto.ds_cep = txtCEP.Text;
            dto.ds_complemento = txtComplemento.Text;

            UsuarioBusiness bus = new UsuarioBusiness();

            bus.Save(dto);

            MessageBox.Show("Salvo com sucesso");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
