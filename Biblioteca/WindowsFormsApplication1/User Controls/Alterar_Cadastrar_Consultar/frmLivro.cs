﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Database.Autor;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;
using WindowsFormsApplication1.DB.ORM.Database.Categoria;
using WindowsFormsApplication1.DB.ORM.Business;

namespace WindowsFormsApplication1.NovasTelas
{
    public partial class frmLivro : UserControl
    {
        public frmLivro()
        {
            InitializeComponent();

            AutorBusiness b = new AutorBusiness();
            List<tb_autor> lista = b.List();
            cboautor.ValueMember = nameof(tb_autor.id_autor);
            cboautor.DisplayMember = nameof(tb_autor.nm_autor);
            cboautor.DataSource = lista;

            CategoriaBusiness c = new CategoriaBusiness();
            List<tb_categoria> lista1 = c.List();
            cbocategoria.ValueMember = nameof(tb_categoria.id_categoria);
            cbocategoria.DisplayMember = nameof(tb_categoria.nm_categoria);
            cbocategoria.DataSource = lista1;

            GeneroBusiness d = new GeneroBusiness();
            List<tb_genero> lista11 = d.List();
            cbogenero.ValueMember = nameof(tb_genero.id_genero);
            cbogenero.DisplayMember = nameof(tb_genero.nm_genero);
            cbogenero.DataSource = lista11;

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            tb_livro dto = new tb_livro();
            //dto.ds_ano =txtano.Text;
            dto.ds_assunto = txtassunto.Text;
            dto.ds_sumario = txtsumario.Text;
            tb_autor autor = cboautor.SelectedItem as tb_autor;
            dto.id_autor = autor.id_autor;
            tb_categoria cat = cbocategoria.SelectedItem as tb_categoria;
            dto.id_categoria = cat.id_categoria;
            tb_genero g = cbogenero.SelectedItem as tb_genero;
            dto.id_gerenro = g.id_genero;
            dto.vl_paginas = int.Parse(txtaginas.Text);
            dto.nm_editora = cboeditora.Text;



            LivroBusiness bus = new LivroBusiness();
            bus.Save(dto);

            MessageBox.Show("Salvo com sucesso");

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
