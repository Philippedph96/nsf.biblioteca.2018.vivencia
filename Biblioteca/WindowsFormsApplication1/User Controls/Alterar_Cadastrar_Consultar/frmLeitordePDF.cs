﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text.pdf.parser;
using WindowsFormsApplication1.User_Controls.Consultar;
using WindowsFormsApplication1.Telas.NewFolder1;

namespace WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar
{
    public partial class frmLeitordePDF : UserControl
    {
        public frmLeitordePDF()
        {
            InitializeComponent();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
           DialogResult re =  MessageBox.Show("As imagens serão ocultadas da vizualização.",
                            "Biblioteca",
                            MessageBoxButtons.OKCancel,
                            MessageBoxIcon.Information);
            if (re == DialogResult.OK)
            {
                LerPDF();
            }

        }
        #region PDF
        public void LerPDF()
        {
            //Abrir o arquivo com um filtro para mostrar apenas pdf
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter = "PDF files |*.pdf", ValidateNames = true })
            {
                //Caso o documento seja aberto 
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        //instancia a api para poder ler o pdf 
                        iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(ofd.FileName);
                        /*StringBuilder aproveita o buffer que já esta sendo utilizado 
                         * ao invés de criar um novo
                         * o buffer não se aplica a objetos ornece
                         * métodos para copiar bytes de uma matriz de tipos 
                         * primitivos para outra matriz de tipos primitivos,
                         * obter um byte de uma matriz, defina um byte em uma matriz
                         * e obter o comprimento de uma matriz. */
                        StringBuilder sb = new StringBuilder();
                        //fro responsavel por mostrar varias paginas seguidas
                        for (int i = 0; i <= reader.NumberOfPages; i++)
                        {
                            //de acordo com o for, le cada pagina 
                            sb.Append(PdfTextExtractor.GetTextFromPage(reader, i));
                        }
                        txtpdf.Text = sb.ToString();
                        reader.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message,
                                         "Biblioteca",
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Error);

                    }
                }
            }

        }

        #endregion

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmConsultar_Livro tela = new frmConsultar_Livro();
            MenuSecundário menu = new MenuSecundário();
            menu.OpenScreen(tela);

        }
    }
}
