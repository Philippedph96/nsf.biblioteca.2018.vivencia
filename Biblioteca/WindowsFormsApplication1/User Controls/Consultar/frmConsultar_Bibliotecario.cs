﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;
using WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar;
using WindowsFormsApplication1.Telas.NewFolder1;

namespace WindowsFormsApplication1.User_Controls.Consultar
{
    public partial class frmConsultar_Bibliotecario : UserControl
    {
        public frmConsultar_Bibliotecario()
        {
            InitializeComponent();
            CarregarDados();
        }

        public void CarregarDados() {
            BibliotecarioBusiness b = new BibliotecarioBusiness();
            List<tb_bibliotecario> lista = b.List();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            frmBibliotecario tela = new frmBibliotecario();
            MenuSecundário menu = new MenuSecundário();
            menu.OpenScreen(tela);
        }
    }
}
