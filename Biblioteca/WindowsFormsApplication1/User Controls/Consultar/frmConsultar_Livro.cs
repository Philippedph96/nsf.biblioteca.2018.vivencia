﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;
using WindowsFormsApplication1.DB.ORM.Business;
using WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar;
using WindowsFormsApplication1.Telas.NewFolder1;

namespace WindowsFormsApplication1.User_Controls.Consultar
{
    public partial class frmConsultar_Livro : UserControl
    {
        public frmConsultar_Livro()
        {
            InitializeComponent();
            CarregarDados();

        }

        public void CarregarDados()
        {
           LivroBusiness b = new LivroBusiness();
            List<tb_livro> lista = b.List();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }

        private void btnregistrar_Click(object sender, EventArgs e)
        {
            frmLeitordePDF tela = new frmLeitordePDF();
            MenuSecundário menu = new MenuSecundário();
            menu.OpenScreen(tela);
        }
    }
}
