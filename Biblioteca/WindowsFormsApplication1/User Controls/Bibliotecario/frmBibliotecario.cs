﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;
using WindowsFormsApplication1.Telas.NewFolder1;
using WindowsFormsApplication1.User_Controls.Consultar;

namespace WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar
{
    public partial class frmBibliotecario : UserControl
    {
        public frmBibliotecario()
        {
            InitializeComponent();
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            tb_bibliotecario dto = new tb_bibliotecario();
            dto.nm_bibliotecario = txtNome.text;
            dto.ds_cpf = txtCPF.text;

            BibliotecarioBusiness bus = new BibliotecarioBusiness();
            bus.Save(dto);

            MessageBox.Show("Salvo com sucesso");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmConsultar_Bibliotecario tela = new frmConsultar_Bibliotecario();
            MenuSecundário menu = new MenuSecundário();
            menu.OpenScreen(tela);
        }
    }
}
