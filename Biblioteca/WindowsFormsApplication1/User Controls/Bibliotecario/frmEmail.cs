﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.Utilitarios.Email;
using System.Net;
using System.Net.Mail;
using S22.Imap;

namespace WindowsFormsApplication1.NovasTelas.Alterar_Cadastrar_Consultar
{
    public partial class frmEmail : UserControl
    {
        public frmEmail()
        {
            InitializeComponent();
        }

        private void btnenviar_Click(object sender, EventArgs e)
        {
            EnviarEmail();
        }


        private void EnviarEmail() {
            //Email email = new Email();
            //#region parametros 
            //string para = txtpara.Text;
            //string copia = txtcc.Text;
            //string assunto = txtassunto.Text;
            //string mensagem = txtMensagem.Text;
            //bool comhtml = chbhtml.Checked;
            //string copiaoculta = copia;

            //#endregion
            //email.EnviarEmail(para, copia, copiaoculta, assunto, mensagem, comhtml);
            //MessageBox.Show("Biblioteca",
            //                "Email Enviado com sucesso.",
            //                MessageBoxButtons.OK,
            //                MessageBoxIcon.Information);

        }
        private void EnviarEmailDeOutroJeito()
        {
            // armazena a mensagem passando o email de quem envia e de quem irá receber
            var message = new MailMessage("Email que está enviando", txtDestinatario.text);
            //assunto é adicionado na mensagem
            message.Subject = txtAssunto.text;
            //adiciona a mensagem na mensagem
            message.Body = txtMensagem.Text;

            //usa o smtpclient para realizar o envio, passando como parametros o smtp do gmail e a porta
            using (SmtpClient mailer = new SmtpClient("smtp.gmail.com", 587))
            {
                //adicionando as credenciais para o envio
                mailer.Credentials = new NetworkCredential("Email que está enviando", "senha");
                //mostra se irá utilizar conexão criptografada
                mailer.EnableSsl = true;
                //envia a mensagem
                mailer.Send(message);
            }
            //limpa todos os campos após enviar
            txtAssunto.text = null;
            txtDestinatario.text = null;
            txtEmail.text = null;
            txtMensagem.Text = null;
        }
    }
}
