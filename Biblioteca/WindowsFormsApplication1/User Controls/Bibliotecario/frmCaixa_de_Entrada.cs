﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Mail;
using S22.Imap;
using WindowsFormsApplication1.Utilitarios.Notificacoes;

namespace WindowsFormsApplication1.User_Controls.Bibliotecario
{
    public partial class frmCaixa_de_Entrada : UserControl
    {
        static frmCaixa_de_Entrada f;

        public frmCaixa_de_Entrada()
        {
            InitializeComponent();
            f = this;
        }

        private void frmCaixa_de_Entrada_Load(object sender, EventArgs e)
        {
            StartReciving();
        }

        private void StartReciving()
        {
            //Tudo indo em conjunto sem esperar ninguem
            Task.Run(() =>
            {
                //utilizando o ImapClient para acessar o gmail através da porta 993, passando
                //email, senha, e o login para ter acesso a caixa 
                using (ImapClient client = new ImapClient("imap.gmail.com",
                                                            993,
                                                            "Email que será lido",
                                                            "senha",
                                                            AuthMethod.Login,
                                                            true))
                {
                    //caso não consiga ler os emails
                    if (client.Supports("IDLE") == false)
                    {
                        Notificacoes notificacoes = new Notificacoes();
                        notificacoes.Aviso("O servidor não está conseguindo suportar esse formato");
                        return;
                    }
                    //adiciona a menssagem 
                    client.NewMessage += new EventHandler<IdleMessageEventArgs>(OnNewMenssage);                    
                    while (true) ;
                }
            });
        }
        static void OnNewMenssage(object sender, IdleMessageEventArgs e)
        {
    
            Notificacoes notificacoes = new Notificacoes();
             notificacoes.NewEmail("Nova mensagem recebida");
    
                //"pegando" os e-mails
            MailMessage m = e.Client.GetMessage(e.MessageUID, FetchOptions.Normal);
           f.Invoke((MethodInvoker)delegate
           {
             f.txtMensagem.AppendText($@" De: {m.From} 
                                          \n Assunto: {m.Subject} 
                                            \n Menssagem: {m.Body}
                                             \n");
           });
        }
    }
}
