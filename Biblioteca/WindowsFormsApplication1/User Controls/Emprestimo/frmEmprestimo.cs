﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.ORM.Business;
using WindowsFormsApplication1.DB.ORM.Database.Bibliotecario;

namespace WindowsFormsApplication1.User_Controls.Emprestimo
{
    public partial class frmEmprestimo : UserControl
    {
        public frmEmprestimo()
        {
            InitializeComponent();
            CarregarDados();


            BibliotecarioBusiness b = new BibliotecarioBusiness();
            List<tb_bibliotecario> lista = b.List();
            cbobibliotecario.ValueMember = nameof(tb_bibliotecario.id_bibliotecario);
            cbobibliotecario.DisplayMember = nameof(tb_bibliotecario.nm_bibliotecario);
            cbobibliotecario.DataSource = lista;

            UsuarioBusiness u = new UsuarioBusiness();
            List<tb_usuario> lista2 = u.List();
            cboleitor.ValueMember = nameof(tb_usuario.id_usuario);
            cboleitor.DisplayMember = nameof(tb_usuario.nm_nome);
            cboleitor.DataSource = lista;

            LivroBusiness l = new LivroBusiness();
            List<tb_livro> lista3 = l.List();
            cbolivro.ValueMember = nameof(tb_livro.id_livro);
            cbolivro.DisplayMember = nameof(tb_livro.nm_livro);
            cbolivro.DataSource = lista3;
        }

        public void CarregarDados()
        {
            EmprestimoBusiness b = new EmprestimoBusiness();
            List<tb_emprestimo> lista = b.List();
            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;

        }

        private void btncategoria_Click(object sender, EventArgs e)
        {
            dados();

        }

        public void dados() {
            tb_emprestimo dto = new tb_emprestimo();
            dto.dt_devolucao = dtdevolucao.Value;
            dto.dt_recebeu = dtemprestimo.Value;
            tb_bibliotecario bi = cbobibliotecario.SelectedItem as tb_bibliotecario;

            dto.id_bibliotecario = bi.id_bibliotecario;
            tb_livro li = cbolivro.SelectedItem as tb_livro;

            dto.id_livro = li.id_livro;

            tb_usuario us = cboleitor.SelectedItem as tb_usuario;
            dto.id_usuario = us.id_usuario;
            EmprestimoBusiness bus = new EmprestimoBusiness();
            bus.Save(dto);

            CarregarDados();
            MessageBox.Show("Salvo com sucesso");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            dados();
        }
    }
}
