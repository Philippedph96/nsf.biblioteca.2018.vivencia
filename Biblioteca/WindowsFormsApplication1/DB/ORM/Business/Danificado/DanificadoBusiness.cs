﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Danificado
{
    public class DanificadoBusiness
    {
        public int Save(tb_danificado dto)
        {
            DanificadoDatabase db = new DanificadoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_danificado dto)
        {
            DanificadoDatabase db = new DanificadoDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            DanificadoDatabase db = new DanificadoDatabase();
            db.Remove(id);
        }

        public List<tb_danificado> List()
        {
            DanificadoDatabase db = new DanificadoDatabase();
            List<tb_danificado> list = db.List();

            return list;
        }
    }
}
