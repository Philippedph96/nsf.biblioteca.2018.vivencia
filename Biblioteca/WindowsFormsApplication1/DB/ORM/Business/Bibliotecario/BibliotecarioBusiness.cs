﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Bibliotecario
{
    public class BibliotecarioBusiness
    {
        public int Save(tb_bibliotecario dto)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            return db.Save(dto);
        }

        public void Update(tb_bibliotecario dto)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            db.Remove(id);
        }

        public List<tb_bibliotecario> List()
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            List<tb_bibliotecario> list = db.List();

            return list;
        }

        public List<tb_bibliotecario> Filter(string bibliotecario)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            List<tb_bibliotecario> filter = db.Filter(bibliotecario);

            return filter;
        }
    }
}
