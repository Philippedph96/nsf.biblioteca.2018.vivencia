﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.ORM.Database;

namespace WindowsFormsApplication1.DB.ORM.Business
{
    public class InventarioBusiness
    {
        public int Save(tb_inventario dto)
        {
            InventarioDatabase db = new InventarioDatabase();
            return db.Save(dto);
        }

        public void Update(tb_inventario dto)
        {
            InventarioDatabase db = new InventarioDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            InventarioDatabase db = new InventarioDatabase();
            db.Remove(id);
        }

        public List<tb_inventario> List()
        {
            InventarioDatabase db = new InventarioDatabase();
            List<tb_inventario> list = db.List();

            return list;
        }

        public List<tb_inventario> Filter(int pesquisa)
        {
            InventarioDatabase db = new InventarioDatabase();
            List<tb_inventario> list = db.Filter(pesquisa);

            return list;
        }
    }
}
