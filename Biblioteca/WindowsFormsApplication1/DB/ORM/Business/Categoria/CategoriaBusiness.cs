﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Categoria
{
    public class CategoriaBusiness
    {
        public int Save(tb_categoria dto)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            return db.Save(dto);
        }

        public void Update(tb_categoria dto)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            db.Remove(id);
        }

        public List<tb_categoria> List()
        {
            CategoriaDatabase db = new CategoriaDatabase();
            List<tb_categoria> list = db.List();

            return list;
        }

        public List<tb_categoria> Filter(string categpria)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            List<tb_categoria> filter = db.Filter(categpria);

            return filter;
        }
    }
}
