﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Punicao
{
    public class PunicaoBusiness
    {
        public int Save(tb_punicao dto)
        {
            PunicaoDatabase db = new PunicaoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_punicao dto)
        {
            PunicaoDatabase db = new PunicaoDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            PunicaoDatabase db = new PunicaoDatabase();
            db.Remove(id);
        }

        public List<tb_punicao> List()
        {
            PunicaoDatabase db = new PunicaoDatabase();
            List<tb_punicao> list = db.List();

            return list;
        }

        public List<tb_punicao> Filter(string punicao)
        {
            PunicaoDatabase db = new PunicaoDatabase();
            List<tb_punicao> filter = db.Filter(punicao);

            return filter;
        }
    }
}
