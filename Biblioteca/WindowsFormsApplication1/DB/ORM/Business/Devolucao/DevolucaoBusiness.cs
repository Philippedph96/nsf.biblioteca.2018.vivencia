﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.ORM.Database.Categoria;

namespace WindowsFormsApplication1.DB.ORM.Database.Devolucao
{
    public class DevolucaoBusiness
    {
        public int Save(tb_devolucao dto)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            return db.Save(dto);
        }

        public void Update(tb_devolucao dto)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            db.Remove(id);
        }

        public List<tb_devolucao> List()
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            List<tb_devolucao> list = db.List();

            return list;
        }

        public List<tb_devolucao> Filter(DateTime date)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            List<tb_devolucao> filter = db.Filter(date);

            return filter;
        }
    }
}
