﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Danificado
{
    public class DanificadoDatabase
    {
        public int Save (tb_danificado dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_danificado.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_danificado dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            context.tb_danificado.First(x => x.id_danificado == id);
            context.SaveChanges();
        }

        public List<tb_danificado> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_danificado.ToList();
        }
    }
}
