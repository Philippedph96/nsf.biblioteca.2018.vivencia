﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Usuario
{
    public class UsuarioDatabase
    {
        public int Save(tb_usuario dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_usuario.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_usuario dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_usuario user = context.tb_usuario.First(x => x.id_usuario == id);
            context.tb_usuario.Remove(user);
            context.SaveChanges();
        }

        public List<tb_usuario> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_usuario.ToList();
        }

        public List<tb_usuario> Filter(string name)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_usuario.Where(x => x.nm_nome.Contains(name)).ToList();
        }
    }
}
