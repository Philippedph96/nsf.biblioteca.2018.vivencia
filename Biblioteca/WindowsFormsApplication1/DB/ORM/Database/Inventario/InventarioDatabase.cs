﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database
{
    public class InventarioDatabase
    {
        public int Save (tb_inventario dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_inventario.Add(dto);
            return context.SaveChanges();
        }

        public void Update (tb_inventario dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove (int id)
        {
            LibraryDB context = new LibraryDB();
            tb_inventario stock = context.tb_inventario.First(x => x.id_inventario == id);
            context.tb_inventario.Remove(stock);
            context.SaveChanges();
        }

        public List<tb_inventario> List ()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_inventario.ToList();
        }

        public List<tb_inventario> Filter (int pesquisa)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_inventario.Where(x => x.ds_pesquisa == pesquisa).ToList();
        }
    }
}
