﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Sessao
{
    public class SessaoDatabase
    {
        public int Save (tb_sessao dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_sessao.Add(dto);
            return context.SaveChanges();
        }

        public void Update (tb_sessao dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
             context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryDB context = new LibraryDB();
            tb_sessao sessao = context.tb_sessao.First(x => x.id_sessao == id);
            context.tb_sessao.Remove(sessao);
            context.SaveChanges();
        }

        public List<tb_sessao> List ()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_sessao.ToList();
        }

        public List<tb_sessao> Filter(string sessao)
        {
            LibraryDB context = new LibraryDB();
            return context.tb_sessao.Where(x => x.nm_sessao.Contains(sessao)).ToList();
        }
    }
}
