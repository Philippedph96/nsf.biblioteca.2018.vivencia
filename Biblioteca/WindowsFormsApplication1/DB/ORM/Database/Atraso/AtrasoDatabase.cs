﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.ORM.Database.Atraso
{
    public class AtrasoDatabase
    {
        public int Save(tb_atraso dto)
        {
            LibraryDB context = new LibraryDB();
            context.tb_atraso.Add(dto);
            return context.SaveChanges();
        }

        public void Update(tb_atraso dto)
        {
            LibraryDB context = new LibraryDB();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove (int id)
        {
            LibraryDB context = new LibraryDB();
            tb_atraso atraso = context.tb_atraso.First(x => x.id_atraso == id);
            context.tb_atraso.Remove(atraso);
            context.SaveChanges();
        }

        public List<tb_atraso> List()
        {
            LibraryDB context = new LibraryDB();
            return context.tb_atraso.ToList();
        }
    }
}
