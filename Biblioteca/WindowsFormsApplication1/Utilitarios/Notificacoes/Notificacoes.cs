﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tulpep.NotificationWindow;

namespace WindowsFormsApplication1.Utilitarios.Notificacoes
{
    class Notificacoes
    {
        #region NewEmail
        public void NewEmail(string qtde){
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.icons8_e_mail_64;
            popup.TitleText = "Biblioteca";
            popup.ContentText = $@"Existem {qtde} novos E-mails.";
            //Show Menssage
            popup.Popup(); 

        }
        #endregion
        #region ListaDeEspera
        public void Espera(string qtde)
        {
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.icons8_livro_100;
            popup.TitleText = "Biblioteca";
            popup.ContentText = $@"Existem {qtde} pessoas na lista de espera por um livro.";
            //Show Menssage
            popup.Popup();

        }
        #endregion
        #region LivrosAtrasados
        public void LivrosAtrasados (string qtde)
        {
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.icons8_literatura_50;
            popup.TitleText = "Biblioteca";
            popup.ContentText = $@"Existem {qtde} livros atrasados.";
            //Show Menssage
            popup.Popup();
        }
        #endregion
        #region LivrosAguardandoReparo
        public void LivrosAguardandoReparo(string qtde)
        {
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.icons8_literatura_50;
            popup.TitleText = "Biblioteca";
            popup.ContentText = $@"Existem {qtde} Livros aguardando reparo.";
            //Show Menssage
            popup.Popup();
        }
        #endregion
        #region Aviso
        public void Aviso(string mensagem)
        {
            PopupNotifier popup = new PopupNotifier();
            popup.Image = Properties.Resources.icons8_literatura_50;
            popup.TitleText = "Biblioteca";
            popup.ContentText = mensagem;
            //Show Menssage
            popup.Popup();
        }
        #endregion

    }
}
