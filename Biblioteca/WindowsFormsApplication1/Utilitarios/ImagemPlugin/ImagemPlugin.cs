﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.Utilitarios.ImagemPlugin
{
    class ImagemPlugin
    {
        public static string ConverterParaString(Image imagem)
        {
           
            MemoryStream memoria = new MemoryStream();
            imagem.Save(memoria, imagem.RawFormat);
            byte[] imagebytes = memoria.ToArray();
            string ImagemEmTexto = Convert.ToBase64String(imagebytes);
            return ImagemEmTexto;
        }

        public static Image ConverterParaImagem(string ImagemEmTexto)
        { 
            byte[] bytes = Convert.FromBase64String(ImagemEmTexto);
            Image imagem = Image.FromStream(new MemoryStream(bytes));
            return imagem;
        }
    }
}
