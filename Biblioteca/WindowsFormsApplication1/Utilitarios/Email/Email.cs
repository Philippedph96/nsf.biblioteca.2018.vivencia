﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;

namespace WindowsFormsApplication1.Utilitarios.Email
{
    class Email
    {
        public void EnviarEmail(string para,
                                string copia,
                                string copiaoculta,
                                string assunto,
                                string mensagem,
                                bool comhtml)
        {
            #region Entrar
            string origem = "";
            string senha = "";
            #endregion

            #region Dados 
            MailMessage email = new MailMessage();
            MailAddress emailPara = new MailAddress(para);
            MailAddress emailCopia = new MailAddress(copia);
            MailAddress emailCopiaOculta = new MailAddress(copiaoculta);
            #endregion

            #region ParametrosMenssagem 
            email.From = new MailAddress(origem);
            email.To.Add(emailPara);
            email.CC.Add(emailCopia);
            email.Bcc.Add(emailCopiaOculta);
            email.Subject = assunto;
            email.Body = mensagem;
            email.IsBodyHtml = comhtml;

            #endregion

            #region Enviando 
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(origem, senha);

            smtp.Send(email);              
            
            #endregion
        }

    }
}
